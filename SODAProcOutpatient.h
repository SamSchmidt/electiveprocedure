//
//  SODAProcOutpatient.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 6/15/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//https://data.cms.gov/Medicare/Outpatient-Prospective-Payment-System-OPPS-Provide/sq7j-n2ta
//sq7j-n2ta

#import <Foundation/Foundation.h>
#import "SODAPropertyMapping.h"

@interface SODAProcOutpatient : NSObject<SODAPropertyMapping> {
    NSString *_procedureName;
    NSString *_providerId;
    NSString *_providerName;
    NSString *_numServices;
    NSString *_submittedCharges;
    NSString *_medicarePayments;
}

#pragma mark - Properties

@property(nonatomic, copy) NSString *procedureName;
@property(nonatomic, copy) NSString *providerId;
@property(nonatomic, copy) NSString *providerName;
@property(nonatomic, copy) NSString *numServices;
@property(nonatomic, copy) NSString *submittedCharges;
@property(nonatomic, copy) NSString *medicarePayments;

@end

