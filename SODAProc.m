//
//  SODAProc.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 3/15/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
// https://data.cms.gov/Medicare/Inpatient-Prospective-Payment-System-IPPS-Provider/97k6-zzx3

#import "SODAProc.h"

//Inpatient Procedures

//https://data.cms.gov/Medicare/Inpatient-Prospective-Payment-System-IPPS-Provider/97k6-zzx3
//97k6-zzx3

/**
 * Model class where SODA Response results get mapped for each one of the matching hospitals
 */
@implementation SODAProc {
    
}

#pragma mark - Properties

@synthesize procedureName = _procedureName;
@synthesize coveredCharges = _coveredCharges;
@synthesize averageMedicarePayments = _averageMedicarePayments;
@synthesize averageMedicarePayments2 = _averageMedicarePayments2;

#pragma mark - SODAPropertyMapping custom mappings impl

/**
 * This custom mapping allows soda response properties to be mapped to object properties with names that do not match those on the
 * remote objects
 */
- (NSDictionary *)propertyMappings {
    return @{
             @"drg_definition" : @"procedureName",
             @"average_covered_charges" : @"coveredCharges",
             @"average_medicare_payments" : @"averageMedicarePayments",
             @"average_medicare_payments_2" : @"averageMedicarePayments2",
             };
}

@end

