//
//  EPLocation.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 4/5/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#import "EPLocation.h"

@interface EPLocation : NSObject <MKAnnotation>

@property (nonatomic) double cost;

- (id)initWithName:(NSString*)name address:(NSString*)address cost:(double)cost coordinate:(CLLocationCoordinate2D)coordinate;
- (MKMapItem*)mapItem;


@end