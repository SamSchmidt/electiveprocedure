//
//  EPInfoTableViewController.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 5/21/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SODAHospital.h"


@interface EPInfoTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property SODAHospital *hospital;

@end
