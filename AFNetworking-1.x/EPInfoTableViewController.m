//
//  EPInfoTableViewController.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 5/21/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import "EPInfoTableViewController.h"

@interface EPInfoTableViewController ()

@property NSArray *cellHeaders;
@property NSArray *cellData;
@property int propsIndex;

@end


@implementation EPInfoTableViewController
{

}

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _propsIndex = 0;
    
    //Formatting help
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    
    [nf setGroupingSeparator:@","];
    [nf setGroupingSize:3];
    [nf setUsesGroupingSeparator:YES];
    [nf setMaximumFractionDigits:0];
    
    if (_hospital.proc.procedureName != nil) {

        _cellData = [NSArray arrayWithObjects:
                        _hospital.proc.procedureName,
                        [NSString stringWithFormat:@"$%@", [nf stringForObjectValue:_hospital.proc.coveredCharges]],
                        [NSString stringWithFormat:@"%@/100", [nf stringFromNumber:_hospital.score.totalPerformanceScore]],
                         [_hospital.address capitalizedString],
                         [_hospital.city capitalizedString],
                         _hospital.state,
                         _hospital.zipCode,
                         _hospital.generalInfo.phoneNumber.phoneNumber,
                         nil];
    }
    else if (_hospital.procOut.procedureName != nil) {
        _cellData = [NSArray arrayWithObjects:
                         _hospital.procOut.procedureName,
                         [NSString stringWithFormat:@"$%@", [nf stringForObjectValue:_hospital.procOut.submittedCharges]],
                         [NSString stringWithFormat:@"%@/100", [nf stringFromNumber:_hospital.score.totalPerformanceScore]],
                         [_hospital.address capitalizedString],
                         [_hospital.city capitalizedString],
                         _hospital.state,
                         _hospital.zipCode,
                         _hospital.generalInfo.phoneNumber.phoneNumber,
                         nil];
    }
    
    _cellHeaders = [NSArray arrayWithObjects:@"Procedure Name",
                    @"Covered Costs",
                    @"Total Performance Score",
                    @"Street Address",
                    @"City",
                    @"State",
                    @"Zip Code",
                    @"Phone Number",
                    nil];
    
//    [cellDatum insertObject: atIndex:0];
    
    //TODO this may be wrong
//    [cellDatum insertObject:_hospital.proc.coveredCharges atIndex:1];
//    
//    [cellDatum insertObject:_hospital.address atIndex:2];
//    //TODO need this
//    [cellDatum insertObject:_hospital.city atIndex:3];
//    [cellDatum insertObject:_hospital.state atIndex:4];
//    [cellDatum insertObject:_hospital.zipCode atIndex:5];
//    [props insertObject:_hospital. atIndex:6];
    
//    NSLog(@"cellDatum:    %@", [cellDatum objectAtIndex:3]);
    NSLog(@"city: %@", _hospital.city);
    
    
    //
    // Change the properties of the imageView and tableView (these could be set
    // in interface builder instead).
    //
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;// UITableViewCellSeparatorStyleNone;
//    _aTableView.rowHeight = 40;
//    _aTableView.backgroundColor = [UIColor whiteColor];
    
    //    _aTableView.image = [UIImage imageNamed:@"gradientBackground.png"];
    
    //
    // Create a header view. Wrap it in a container to allow us to position
    // it better.
    //
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];// autorelease];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 40)];// autorelease];
    headerLabel.text = _hospital.hospitalName;
    headerLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    
    
    headerLabel.textColor = [UIColor colorWithRed:0 green:122.0/255.0 blue:255 alpha:1.0];
//    headerLabel.shadowColor = [UIColor whiteColor];
//    headerLabel.shadowOffset = CGSizeMake(0, 1);
//    headerLabel.font = [UIFont boldSystemFontOfSize:22];

    headerLabel.adjustsFontSizeToFitWidth = YES;
//    headerLabel.minimumFontSize = 0;
    
    
    headerLabel.backgroundColor = [UIColor clearColor];
    [containerView addSubview:headerLabel];
    
    self.tableView.tableHeaderView = containerView;
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;

    return sectionName;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = NSLocalizedString(@"General Information", @"Generics");
            break;
        case 1:
            sectionName = NSLocalizedString(@"Contact Information", @"Contact Information");
            break;
        default:
            sectionName = @"";
            break;
    }    /* Section header is in 0th index... */
    
    
    [label setText:sectionName];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0]];
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger *numRows = 0;
    switch (section)
    {
        case 0:
            numRows = 3;
            break;
        case 1:
            numRows = 5;
            break;
        default:
            numRows = 0;
            break;
    }
    
    return numRows;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *simpleTableIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    if ( _propsIndex < [_cellData count] ) {
        cell.textLabel.text = [_cellHeaders objectAtIndex:_propsIndex];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [_cellData objectAtIndex:_propsIndex]];
    }
    _propsIndex++;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
