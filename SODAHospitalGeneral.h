//
//  SODAHospitalGeneral.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 6/14/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//General Hospital Information
//https://data.medicare.gov/Hospital-Compare/Hospital-General-Information/v287-28n3
//v287-28n3
//new:
//https://data.medicare.gov/Hospital-Compare/Hospital-General-Information/xubh-q36u
//xubh-q36u


#import <Foundation/Foundation.h>
#import "SODAPropertyMapping.h"
#import "SODAPhone.h"

@interface SODAHospitalGeneral : NSObject<SODAPropertyMapping> {
    NSString *_providerNumber;
    NSString *_hospitalName;
    NSString *_address;
    NSString *_city;
    NSString *_state;
    NSString *_zipCode;
    NSString *_county;
    SODAPhone *_phoneNumber;
    NSString *_hospitalType;
    NSString *_hospitalOwner;
}

#pragma mark - Properties

@property(nonatomic, copy) NSString *providerNumber;
@property(nonatomic, copy) NSString *hospitalName;
@property(nonatomic, copy) NSString *address;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *state;
@property(nonatomic, copy) NSString *zipCode;
@property(nonatomic, copy) NSString *county;
@property(nonatomic, strong) SODAPhone *phoneNumber;
@property(nonatomic, copy) NSString *hospitalType;
@property(nonatomic, copy) NSString *hospitalOwner;


@end