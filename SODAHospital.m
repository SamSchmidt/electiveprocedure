//
//  SODAHospital.m
//  ElectiveProcedure
//
//  Created by Samuel Schmidt on 1/27/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//https://data.medicare.gov/Hospital-Compare/Hospital-Value-Based-Purchasing-HVBP-Patient-Exper/avtz-f2ge
//avtz-f2ge

#import "SODAHospital.h"
#import <CoreLocation/CoreLocation.h>


/**
 * Model class where SODA Response results get mapped for each one of the matching hospitals
 */
@implementation SODAHospital {
    
}

#pragma mark - Properties

@synthesize providerNumber = _providerNumber;
@synthesize hospitalName = _hospitalName;
@synthesize address = _address;
@synthesize city = _city;
@synthesize state = _state;
@synthesize zipCode = _zipCode;
@synthesize countyName = _countyName;
@synthesize communicationWithNursesDimensionScore = _communicationWithNursesDimensionScore;
@synthesize communicationWithDoctorsDimensionScore = _communicationWithDoctorsDimensionScore;
@synthesize responsivenessOfHospitalStaffDimensionScore = _responsivenessOfHospitalStaffDimensionScore;
@synthesize communicationAboutMedicinesDimensionScore = _communicationAboutMedicinesDimensionScore;
@synthesize dischargeInformationDimensionScore = _dischargeInformationDimensionScore;
@synthesize overallRatingOfHospitalDimensionScore = _overallRatingOfHospitalDimensionScore;
@synthesize location = _location;
@synthesize overallScore = _overallScore;

#pragma mark - SODAPropertyMapping custom mappings impl

/**
 * This custom mapping allows soda response properties to be mapped to object properties with names that do not match those on the
 * remote objects
 */
- (NSDictionary *)propertyMappings {
    return @{
             @"provider_number" : @"providerNumber",
             @"hospital_name" : @"hospitalName",
             @"address" : @"address",
             @"city" : @"city",
             @"state" : @"state",
             @"zip_code" : @"zipCode",
             @"county_name" : @"countyName",
             @"communication_with_nurses_dimension_score" : @"communicationWithNursesDimensionScore",
             @"communication_with_doctors_dimension_score" : @"communicationWithDoctorsDimensionScore",
             @"responsiveness_of_hospital_staff_dimension_score" : @"responsivenessOfHospitalStaffDimensionScore",
             @"communication_about_medicines_dimension_score" : @"communicationAboutMedicinesDimensionScore",
             @"discharge_information_dimension_score" : @"dischargeInformationDimensionScore",
             @"overall_rating_of_hospital_dimension_score" : @"overallRatingOfHospitalDimensionScore",
             @"location" : @"location",
             @"hcahps_base_score" : @"overallScore",
             };
}


@end