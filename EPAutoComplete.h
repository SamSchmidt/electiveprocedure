//
//  EPAutoComplete.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 3/15/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPAutoComplete : NSObject

@property NSMutableArray *allProcedures;


- (void)loadProcedureNames;

@end
