//
//  SODAHospitalScore.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 6/14/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//https://data.medicare.gov/Hospital-Compare/Hospital-Value-Based-Purchasing-HVBP-Total-Perform/ypbt-wvdk
//ypbt-wvdk

#import <Foundation/Foundation.h>
#import "SODAPropertyMapping.h"

@interface SODAHospitalScore : NSObject<SODAPropertyMapping> {
    NSString *_providerNumber;
    NSString *_clinicalProcessScoreUnweighted;
    NSString *_clinicalProcessScoreWeighted;
    NSString *_patientExperienceUnweighted;
    NSString *_patientExperienceWeighted;
    NSString *_outcomeUnweighted;
    NSString *_outcomeWeighted;
    NSNumber *_totalPerformanceScore;

}

#pragma mark - Properties

@property(nonatomic, copy) NSString *providerNumber;
@property(nonatomic, copy) NSString *clinicalProcessScoreUnweighted;
@property(nonatomic, copy) NSString *clinicalProcessScoreWeighted;
@property(nonatomic, copy) NSString *patientExperienceUnweighted;
@property(nonatomic, copy) NSString *patientExperienceWeighted;
@property(nonatomic, copy) NSString *outcomeUnweighted;
@property(nonatomic, copy) NSString *outcomeWeighted;
@property(nonatomic, copy) NSNumber *totalPerformanceScore;

@end