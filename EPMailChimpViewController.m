////
////  EPMailChimpViewController.m
////  ElectiveProcedure3
////
////  Created by Samuel Schmidt on 8/24/14.
////  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
////
//
//#import "EPMailChimpViewController.h"
//
//
//#import "Lib/ChimpKit/Core/Classes/SubscribeAlertView.h"
//#import "Lib/ChimpKit/Core/Classes/CKAuthViewController.h"
//#import "Lib/ChimpKit/Core/Classes/ChimpKit.h"
//
//@interface EPMailChimpViewController ()
//
//@end
//
//@implementation EPMailChimpViewController
//
///*
// // The designated initializer. Override to perform setup that is required before the view is loaded.
// - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
// self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
// if (self) {
// // Custom initialization
// }
// return self;
// }
// */
//
///*
// // Implement loadView to create a view hierarchy programmatically, without using a nib.
// - (void)loadView {
// }
// */
//
//
///*
// // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
// - (void)viewDidLoad {
// [super viewDidLoad];
// }
// */
//
//
///*
// // Override to allow orientations other than the default portrait orientation.
// - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
// // Return YES for supported orientations
// return (interfaceOrientation == UIInterfaceOrientationPortrait);
// }
// */
//
//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    
//    SubscribeAlertView *alert = [[SubscribeAlertView alloc] initWithTitle:@"Subscribe"
//                                                                  message:@"Enter your email address to subscribe to our mailing list."
//                                                                   apiKey:@"<YOUR API KEY>"
//                                                                   listId:@"<LIST ID>"
//                                                        cancelButtonTitle:@"Cancel"
//                                                     subscribeButtonTitle:@"Subscribe"];
//    //    [alert show];
//    
//    
//    //Authenticating via OAuth2
//    
//    if (!shownAuthView) {
//        shownAuthView = YES;
//        
//        //You don't have to use a navigation controller, but we'll put a cancel button on it for you if you do
//        CKAuthViewController *authViewController = [[CKAuthViewController alloc] initWithClientId:@"<YOUR_CLIENT_ID>" andClientSecret:@"<YOUR_CLIENT_SECRET>"];
//        authViewController.delegate = self;
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:authViewController];
//        //        [self presentModalViewController:navigationController animated:YES];
//    }
//}
//
//- (void)ckAuthSucceededWithApiKey:(NSString *)apiKey {
//    NSLog(@"Auth success - api key is: %@", apiKey);
//}
//
//- (void)ckAuthFailedWithError:(NSError *)error {
//    NSLog(@"Auth failed - error is: %@", error);
//}
//
//- (void)ckAuthUserDismissedView {
//    NSLog(@"User dismissed view");
//}
//
//- (void)didReceiveMemoryWarning {
//	// Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//	
//	// Release any cached data, images, etc that aren't in use.
//}
//
//- (void)viewDidUnload {
//	// Release any retained subviews of the main view.
//	// e.g. self.myOutlet = nil;
//}
//
//
////
////end mailchimp provided code
////
//
//
////- (id)initWithStyle:(UITableViewStyle)style
////{
////    self = [super initWithStyle:style];
////    if (self) {
////        // Custom initialization
////    }
////    return self;
////}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    // Uncomment the following line to preserve selection between presentations.
//    // self.clearsSelectionOnViewWillAppear = NO;
//    
//    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//}
//
////- (void)didReceiveMemoryWarning
////{
////    [super didReceiveMemoryWarning];
////    // Dispose of any resources that can be recreated.
////}
//
//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}
//
///*
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
//    
//    // Configure the cell...
//    
//    return cell;
//}
//*/
//
///*
//// Override to support conditional editing of the table view.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the specified item to be editable.
//    return YES;
//}
//*/
//
///*
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }   
//}
//*/
//
///*
//// Override to support rearranging the table view.
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
//{
//}
//*/
//
///*
//// Override to support conditional rearranging of the table view.
//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the item to be re-orderable.
//    return YES;
//}
//*/
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//@end
