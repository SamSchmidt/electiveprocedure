//
//  SODAHospitalScore.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 6/14/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//https://data.medicare.gov/Hospital-Compare/Hospital-Value-Based-Purchasing-HVBP-Total-Perform/ypbt-wvdk
//ypbt-wvdk

#import "SODAHospitalScore.h"


@implementation SODAHospitalScore {
    
}

/**
 * Model class where SODA Response results get mapped for each one of the matching hospitals
 */

#pragma mark - Properties

@synthesize providerNumber = _providerNumber;
@synthesize clinicalProcessScoreUnweighted = _clinicalProcessScoreUnweighted;
@synthesize clinicalProcessScoreWeighted = _clinicalProcessScoreWeighted;
@synthesize patientExperienceUnweighted = _patientExperienceUnweighted;
@synthesize patientExperienceWeighted = _patientExperienceWeighted;
@synthesize outcomeUnweighted = _outcomeUnweighted;
@synthesize outcomeWeighted = _outcomeWeighted;
@synthesize totalPerformanceScore = _totalPerformanceScore;


#pragma mark - SODAPropertyMapping custom mappings impl

/**
 * This custom mapping allows soda response properties to be mapped to object properties with names that do not match those on the
 * remote objects
 */
- (NSDictionary *)propertyMappings {
    return @{
             @"provider_number" : @"providerNumber",
             @"unweighted_normalized_clinical_process_of_care_domain_score" : @"clinicalProcessScoreUnweighted",
             @"weighted_clinical_process_of_care_domain_score" : @"clinicalProcessScoreWeighted",
             @"unweighted_patient_experience_of_care_domain_score" : @"patientExperienceUnweighted",
             @"weighted_patient_experience_of_care_domain_score" : @"patientExperienceWeighted",
             @"unweighted_normalized_outcome_domain_score" : @"outcomeUnweighted",
             @"weighted_outcome_domain_score" : @"outcomeWeighted",
             @"total_performance_score" : @"totalPerformanceScore",
             };
}

@end

