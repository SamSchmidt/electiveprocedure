//
//  SODAProcOut.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 7/19/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SODAPropertyMapping.h"

@interface SODAProcOut : NSObject<SODAPropertyMapping> {
    NSString *_procedureName;
    NSString *_submittedCharges;
    NSNumber *_totalPayments;
    
}

#pragma mark - Properties

@property(nonatomic, copy) NSString *procedureName;
@property(nonatomic, copy) NSString *submittedCharges;
@property(nonatomic, copy) NSNumber *totalPayments;


@end
