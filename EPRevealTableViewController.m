//
//  EPRevealTableViewController.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 8/5/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import "EPRevealTableViewController.h"

#import "EPViewController.h"
#import "SWRevealViewController.h"

@interface EPRevealTableViewController ()
@property (weak, nonatomic) IBOutlet UITableViewCell *aboutLabel;

@end

@implementation EPRevealTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Get the top level controller
    SWRevealViewController *sw = (SWRevealViewController*)self.parentViewController;
    
    //Get the main page (search page) view controller and cast it
    UIViewController *vc = (UIViewController*)sw.frontViewController;
    EPViewController *ep = [vc.childViewControllers objectAtIndex:0];

    NSLog(@"%@", vc.childViewControllers);
    
    //Close the slideout page
    [sw revealToggle:self];
    //            [ep.sidebarButton sendActionsForControlEvents: UIControlEventTouchUpInside];
    
    switch (indexPath.row) {
        case 0: {
            //list of procs
        }
        case 1: {
            [ep performSegueWithIdentifier:@"main_about" sender:ep];
            break;
        }
        case 2: {
            [ep performSegueWithIdentifier:@"main_contact" sender:ep];
            break;
        }
        case 3: {
            [ep performSegueWithIdentifier:@"main_data_sources" sender:ep];
            break;
        }
        case 4: {
            [ep performSegueWithIdentifier:@"main_disclaimer" sender:ep];
            break;
        }
        default: {
        
        }
    }
}



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
