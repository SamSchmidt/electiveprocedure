TranspRx is an abandoned project by Samuel Schmidt. Concept by Abhi Kumbara.

If you wish to see source code, click "source" on the left. Most of the important files are in the root directory. The main class is EPViewController.m. EP (for Elective Procedure) is the class prefix for the project. 


This iOS app is designed to assist those in need of elective medical procedures by helping them find the hospital that best meets their needs. The user can specify which care metrics are most important to them, and we will show them the best (and least expensive) hospitals in their area.

The main requirements we will help fulfill are as follows:
	1) Location
	2) Cost
	3) Personalized Requests, such as bedside care.

Our data sources are listed below.

https://data.cms.gov/Medicare/Inpatient-Prospective-Payment-System-IPPS-Provider/97k6-zzx3

https://data.cms.gov/Medicare/Outpatient-Prospective-Payment-System-OPPS-Provide/sq7j-n2ta

https://data.medicare.gov/Hospital-Compare/Hospital-Value-Based-Purchasing-HVBP-Patient-Exper/avtz-f2ge

https://data.medicare.gov/Hospital-Compare/Hospital-General-Information/v287-28n3

https://data.medicare.gov/Hospital-Compare/Hospital-Value-Based-Purchasing-HVBP-Total-Perform/ypbt-wvdk

https://data.cms.gov/Medicare/Medicare-Provider-Utilization-and-Payment-Data-Phy/jzd2-pt4g

https://data.medicare.gov/Hospital-Compare/Hospital-General-Information/xubh-q36u