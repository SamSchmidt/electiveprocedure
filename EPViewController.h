//
//  EPViewController.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 2/12/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import <MLPAutoCompleteTextField/MLPAutoCompleteTextField.h>

@interface EPViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *autoCompleteTextField;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (weak, nonatomic) IBOutlet UISegmentedControl *searchRadius;

@property UIActivityIndicatorView *indicator;

@end
