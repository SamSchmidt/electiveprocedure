//
//  SODAProcOutpatient.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 6/15/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//https://data.cms.gov/Medicare/Outpatient-Prospective-Payment-System-OPPS-Provide/sq7j-n2ta
//sq7j-n2ta

#import "SODAProcOutpatient.h"


/**
 * Model class where SODA Response results get mapped for each one of the matching hospitals
 */
@implementation SODAProcOutpatient {
    
}

#pragma mark - Properties

@synthesize procedureName = _procedureName;
@synthesize providerId = _providerId;
@synthesize providerName = _providerName;
@synthesize numServices = _numServices;
@synthesize submittedCharges = _submittedCharges;
@synthesize medicarePayments = _medicarePayments;

#pragma mark - SODAPropertyMapping custom mappings impl

/**
 * This custom mapping allows soda response properties to be mapped to object properties with names that do not match those on the
 * remote objects
 */
- (NSDictionary *)propertyMappings {
    return @{
             @"apc" : @"procedureName",
             @"provider_id" : @"providerId",
             @"provider_name" : @"providerName",
             @"outpatient_services" : @"numServices",
             @"average_estimated_submitted_charges" : @"submittedCharges",
             @"average_medicare_payments" : @"medicarePayments",
             };
}

@end


