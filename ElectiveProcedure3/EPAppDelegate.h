//
//  EPAppDelegate.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 2/10/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
