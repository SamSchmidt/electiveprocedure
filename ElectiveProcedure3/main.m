//
//  main.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 2/10/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EPAppDelegate class]));
    }
}
