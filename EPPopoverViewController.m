//
//  EPPopoverViewController.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 4/13/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import "EPPopoverViewController.h"
#import "SODAHospital.h"
#import "EPMapViewController.h"

#import "SODALocation.h"

@interface EPPopoverViewController ()

@end

@implementation EPPopoverViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)addHospital:(SODAHospital *)hosp
{
    self.hospital = hosp;
    
//    [item release];
    
    return self;
}

- (void)setMapViewController:(EPMapViewController *)mvc
{
    _backgroundMapController = mvc;
}


//Event handlers
- (IBAction)buttonTouchDown:(id)sender {
    
    UIButton *myButton = (UIButton *)sender;
    
    [UIView animateWithDuration:0.25 animations:^{myButton.alpha = 0.5;}];
    
}

- (IBAction)buttonTouchUpOutside:(id)sender {
    
    UIButton *myButton = (UIButton *)sender;
    
    [UIView animateWithDuration:0.25 animations:^{myButton.alpha = 1.0;}];
}


- (IBAction)infoButtonTouchUpInside:(id)sender {
    
    [self buttonTouchUpOutside:sender];

    
//    NSLog(@"button press: %@", epMap.hospitals);
    
//Dismiss the popover (save it?)
//    [_aPopover dismissPopoverAnimated:TRUE];
    [_backgroundMapController dismissPopupAndSave];
    
    [_backgroundMapController performSegueWithIdentifier:@"mapToInfoPageSegue" sender:self];
}

- (IBAction)callButtonTouchUpInside:(UIButton *)sender {
    
    [self buttonTouchUpOutside:sender];
    
    NSString *phoneURL = [NSString stringWithFormat:@"tel:%@", _hospital.generalInfo.phoneNumber.phoneNumber];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneURL]];
}

- (IBAction)directionsButtonTouchUpInside:(id)sender {
    
    [self buttonTouchUpOutside:sender];
    
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(_hospital.location.latitude.doubleValue,
                                                                       _hospital.location.longitude.doubleValue);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:_hospital.hospitalName];
        
        
        //Add Directions option
        NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
        [launchOptions setValue:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
        
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:launchOptions];
    }
//    If you want to get driving or walking instructions to the location, you can include a o with the MKMapItem in the array in +openMapsWithItems:launchOptions:, and set the launch options appropriately.
//    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *formattedOutput;
    NSNumberFormatter *nf;
    NSNumber *cost;
    
    
    if (_hospital.proc.procedureName != nil) {
        cost = [[NSNumber alloc] initWithDouble:(_hospital.proc.coveredCharges.doubleValue)];
    }
    else if (_hospital.procOut.procedureName != nil) {
        cost = [[NSNumber alloc] initWithDouble:_hospital.procOut.submittedCharges.doubleValue];
    }
    else return;
        
    nf = [[NSNumberFormatter alloc] init];

    [nf setGroupingSeparator:@","];
    [nf setGroupingSize:3];
    [nf setUsesGroupingSeparator:YES];
    [nf setMaximumFractionDigits:0];
    
//    NSNumberFormatter nfScore = [[NSNumberFormatter alloc] init];
//    [nfScore setMaximumFractionDigits:0];
    
    formattedOutput = [nf stringFromNumber:cost];
    
    
    //Display information in the popup
    _navBar.topItem.title = @"";
    
    
    
    _labelTitle.text = _hospital.hospitalName;
    _labelTitle.adjustsFontSizeToFitWidth = YES;
    
    
//    if (_hospital.proc.procedureName != nil) {
        _labelOne.text = [NSString stringWithFormat:@"Average Cost: $%@", formattedOutput];
//    }
//    else if (_hospital.procOut.procedureName != nil) {
//        _labelOne.text = [NSString stringWithFormat:@"Average Cost: $%@", formattedOutput];
//    }
    
        
    [nf setUsesGroupingSeparator:NO];

    formattedOutput = [nf stringFromNumber:_hospital.score.totalPerformanceScore];
    _labelTwo.text = [NSString stringWithFormat:@"Total Performance Score: %@/100", formattedOutput];


    
    NSLog(@"overallScore: %f", _hospital.overallScore.doubleValue);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
