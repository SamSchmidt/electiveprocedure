//
//  EPContactViewController.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 8/24/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import "EPContactViewController.h"

@interface EPContactViewController ()

@end

@implementation EPContactViewController

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Event handlers
- (IBAction)buttonTouchDown:(id)sender {
    
    UIButton *myButton = (UIButton *)sender;
    
    [UIView animateWithDuration:0.25 animations:^{myButton.alpha = 0.5;}];
    
}

- (IBAction)buttonTouchUpOutside:(id)sender {
    
    UIButton *myButton = (UIButton *)sender;
    
    [UIView animateWithDuration:0.25 animations:^{myButton.alpha = 1.0;}];
}


- (IBAction)touchUpInsideFacebook:(id)sender {
    
    NSLog(@"Facebook touch");
    [self buttonTouchUpOutside:sender];

    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/1515141362033497"];
    if ( [[UIApplication sharedApplication] canOpenURL:facebookURL] ) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/TranspRx"]];
    }
    
    
}

- (IBAction)touchUpInsideMail:(id)sender {
    
    NSLog(@"Mail touch");
    [self buttonTouchUpOutside:sender];
    
    NSURL *mailURL = [NSURL URLWithString:@"mailto:TranspRxApp@gmail.com?subject=Comment,%20Question,%20or%20Concern"];
    
    if ( [[UIApplication sharedApplication] canOpenURL:mailURL] )
        [[UIApplication sharedApplication] openURL: mailURL];
    else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"ALERT"
                              message:@"Please install an email application in order to send a message to the TranspRx team."
                              delegate:self
                              cancelButtonTitle:@"Continue"
                              otherButtonTitles:nil, nil];
        [alert show];
    }


}

- (IBAction)touchUpInsideMailChimp:(id)sender {

    NSLog(@"MailChimp touch");
    [self buttonTouchUpOutside:sender];
    
    NSURL *mailChimpURL = [NSURL URLWithString:@"https://www.facebook.com/TranspRx/app_100265896690345"];
    [[UIApplication sharedApplication] openURL:mailChimpURL];

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
