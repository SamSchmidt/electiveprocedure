//
//  EPAutoComplete.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 3/15/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import "EPAutoComplete.h"
#import "MLPAutoCompleteTextField.h"

#import "SODAProc.h"
#import "SODAProcOut.h"

//SODA files
#import "SODAConsumer.h"
#import "SODAQuery.h"
#import "SODAResponse.h"
#import "SODACallback.h"
#import "SODAError.h"

@implementation EPAutoComplete

- (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
      possibleCompletionsForString:(NSString *)string
{
    return _allProcedures;
    
//    return [self allProcedures];
}

- (void) loadProcedureNames
{
    SODAConsumer *consumer = [SODAConsumer consumerWithDomain:@"data.cms.gov" token:nil];
    
    _allProcedures = [[NSMutableArray alloc]init];
    
    [consumer getObjectsInDataset:@"97k6-zzx3" forQuery:@"select drg_definition group by drg_definition"
                        mappingTo:[SODAProc class]
                           result:[SODACallback callbackWithResult:^(SODAResponse *response) {
        if (response.hasError) {
            NSLog(@"\nError : %@", response.error.message);
        }
        else {
            NSArray* procs = (NSArray*)response.entity;
            
            for (int index=0; index<[procs count]; index++) {
                NSString * str = [[[procs objectAtIndex:index] valueForKey:@"procedureName"] capitalizedString];
                [_allProcedures addObject:str];
            }
        }
    }]];
    
    [consumer getObjectsInDataset:@"tr34-anpb" forQuery:@"select apc group by apc"
                        mappingTo:[SODAProcOut class]
                           result:[SODACallback callbackWithResult:^(SODAResponse *response) {
        if (response.hasError) {
            NSLog(@"\nError : %@", response.error.message);
        }
        else {
            NSArray* procsOut = (NSArray*)response.entity;
            
            [_allProcedures addObjectsFromArray:[procsOut valueForKey:@"procedureName"]];
            
        }
    }]];
}

@end
