//
//  EPViewController.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 2/12/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "EPViewController.h"
#import "EPMapViewController.h"
#import "EPAutoComplete.h"

#import "SWRevealViewController.h"


#import <MLPAutoCompleteTextField/MLPAutoCompleteTextField.h>

#import "stdio.h"
#import "glpk.h"

#import <QuartzCore/QuartzCore.h>

//SODA files
#import "SODAConsumer.h"
#import "SODAQuery.h"
#import "SODAResponse.h"
#import "SODACallback.h"
#import "SODAError.h"


//My SODA files
#import "SODAHospital.h"
#import "SODAProc.h"
#import "SODAHospitalGeneral.h"
#import "SODAHospitalScore.h"

#define NUM_SODA_CALLS 1
#define NUM_SOLVER_CONSTRAINTS 7
#define APC_LENGTH 5 //actual code length + 1
#define DRG_LENGTH 4

@interface EPViewController ()

@property (weak, nonatomic) IBOutlet UIButton *searchButton;


@property NSArray *hospitals;
@property NSInteger sodaCallsRemaining;

@property EPAutoComplete *epAutoComplete;

@property BOOL useLocationServices;

@property CLLocationManager *locationManager;

@property int range;

@property NSInteger numProcsToGet;
@property NSInteger numProcsGot;

@end

@implementation EPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }

    //_searchBox.autoCompleteDataSource = new class;
    NSLog(@"in initWithNibName");

    return self;
}

- (void)viewWillAppear:(BOOL)animated {
//    NSLog(@"view will appear 1st view");
    //Maybe put formatting/appearance things here?
    
    
    _numProcsToGet=0;
    _numProcsGot=0;
    
//    [[UINavigationItem appearance] setBarTintColor:[UIColor redColor]];
}

- (void)viewDidAppear:(BOOL)animated {
    //"animate" textfield down to resting position
//    [UIView beginAnimations:nil context:NULL];
//	[UIView setAnimationDelegate:self];
//	[UIView setAnimationDuration:0.5];
//	[UIView setAnimationBeginsFromCurrentState:YES];
////	self.autoCompleteTextField.frame = CGRectMake(self.autoCompleteTextField.frame.origin.x,
//                                                  200,
//                                                  self.autoCompleteTextField.frame.size.width,
//                                                  self.autoCompleteTextField.frame.size.height);
//	[UIView commitAnimations];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    

    NSLog(@"In viewController viewDidLoad");

    _hospitals = NULL;
    _sodaCallsRemaining = NUM_SODA_CALLS;
    
    self.autoCompleteTextField.delegate = self;
    
    //Put outline around search button
    _searchButton.layer.borderWidth=0.2f;
    _searchButton.layer.borderColor=[[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] CGColor];
    _searchButton.layer.cornerRadius = 7;
    
    
    //MLP Autocomplete
    
    //Supported Styles for autocomplete:
//    [self.autocompleteTextField setBorderStyle:UITextBorderStyleBezel];
//    [self.autoCompleteTextField setBorderStyle:UITextBorderStyleLine];
//    [self.autoCompleteTextField setBorderStyle:UITextBorderStyleNone];
    [self.autoCompleteTextField setBorderStyle:UITextBorderStyleRoundedRect];
    
    [self.autoCompleteTextField setShowAutoCompleteTableWhenEditingBegins:YES];
    [self.autoCompleteTextField setReverseAutoCompleteSuggestionsBoldEffect:YES];
//
    [_autoCompleteTextField setAutoCompleteTableBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
//    [_autoCompleteTextField setAutoCompleteTableBackgroundColor:[UIColor colorWithRed:.7 green:1 blue:1 alpha:.9]];
    
    //TODO Formatting
//    [_autoCompleteTextField autoCompleteBoldFontName]


    
    //You can use custom TableViewCell classes and nibs in the autocomplete tableview if you wish.
    //This is only supported in iOS 6.0, in iOS 5.0 you can set a custom NIB for the cell
    if ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] != NSOrderedAscending) {
//        [self.autocompleteTextField registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
//                                           forCellReuseIdentifier:@"CustomCellId"];
    }
    else if ([[[UIDevice currentDevice] systemVersion] compare:@"5.0" options:NSNumericSearch] != NSOrderedAscending) {
        //Turn off bold effects on iOS 5.0 as they are not supported and will result in an exception
//        self.autoCompleteTextField.applyBoldEffectToAutoCompleteSuggestions = NO;

    }
    
//    _autoCompleteTextField.sortAutoCompleteSuggestionsByClosestMatch = TRUE;
    
    _epAutoComplete = [[EPAutoComplete alloc] init];
    [_epAutoComplete loadProcedureNames];
    
    self.autoCompleteTextField.autoCompleteDataSource = (id)_epAutoComplete;
    
    
    //Location Services
    _useLocationServices =  [CLLocationManager locationServicesEnabled];
    
    if (_useLocationServices) {
        _locationManager = [[CLLocationManager alloc ] init ];
        
        _locationManager.delegate = (id)self;
        
        //Use in production app
        //[_locationManager startMonitoringSignificantLocationChanges];
        
        //For simulator testing use:
        [_locationManager startUpdatingLocation];
        
    }
    
    // Change button color
    //_sidebarButton.tintColor = [UIColor colorWithWhite:0.96f alpha:0.2f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/////////////////////
/////myFunctions/////
/////////////////////

//Closes the keyboard when the user touches "Done"
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.autoCompleteTextField) {
        [theTextField resignFirstResponder];
        }
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //hides keyboard when another part of layout was touched
    
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

#define kOFFSET_FOR_KEYBOARD 80.0

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    if ([sender isEqual:self.autoCompleteTextField])
//    {
//        //move the main view, so that the keyboard does not hide it.
//        if  (self.view.frame.origin.y >= 0)
//        {
//            [self setViewMovedUp:YES];
//        }
//    }
//}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)locationManager:(CLLocationManager *)manager
        didUpdateLocations:(NSArray *)locations
{
//    for (int i=0; i<[locations count]; i++)
//    {
////        NSLog(@"new location is: %@", locations[i]);
//    }
    
//    NSLog(@"now app location is: %@", _locationManager.location);
    
}


//Get the hospitals within range of the user
//Send this list of hospitals to viewController
- (void)getHospitalsWithinRange {
    //SODA variables for query by location (geobox)
    SODAConsumer *consumer = [SODAConsumer consumerWithDomain:@"data.medicare.gov" token:nil];
    SODAQuery *query = [SODAQuery queryWithDataset:@"avtz-f2ge" mapping:[SODAHospital class]];

    //Get the current location
    CLLocationCoordinate2D cld = _locationManager.location.coordinate;
    
    CLLocationCoordinate2D northC = [self coordinateFromCoord:cld atDistanceKm:_range atBearingDegrees:0];
    CLLocationCoordinate2D eastC = [self coordinateFromCoord:cld atDistanceKm:_range atBearingDegrees:270];
    CLLocationCoordinate2D southC = [self coordinateFromCoord:cld atDistanceKm:_range atBearingDegrees:180];
    CLLocationCoordinate2D westC = [self coordinateFromCoord:cld atDistanceKm:_range atBearingDegrees:90];
    
    NSNumber *north = [[NSNumber alloc ] initWithDouble:(northC.latitude)];
    NSNumber *east = [[NSNumber alloc ] initWithDouble:(eastC.longitude)];
    NSNumber *south = [[NSNumber alloc ] initWithDouble:(southC.latitude)];;
    NSNumber *west = [[NSNumber alloc ] initWithDouble:(westC.longitude)];
    
    
    NSLog(@"cld lat long: %f   %f", cld.latitude, cld.longitude);
    
    //Add the geobox to the query
    [query where:@"location" withinBox:geoBox(north, east, south, west)];
    
    //Get hospitals near the user!
    [consumer getObjectsForTypedQuery:query result:[SODACallback callbackWithResult:^(SODAResponse *response) {
        NSLog(@"json response : %@", response.json);
        if (response.hasError) {
            NSLog(@"\nError : %@", response.error.message);
        } else {
            
            _hospitals = (NSArray*)response.entity;
            //NSLog(@"ResultInside : %@", _hospitals);
            NSLog(@"ResultInsideCount : %lu", (unsigned long)_hospitals.count);
            
            _numProcsToGet = 2*_hospitals.count;
        
            //get cost associated with the procedure for each hospital
            for (int i=0; i<_hospitals.count; i++) {
                [self getInfoForHospital:[_hospitals objectAtIndex:i]];
            }
//            [self performSegueWithIdentifier:@"mySegue" sender:self];
            
            
            
            if (_hospitals.count == 0) {
                [_indicator stopAnimating];
              
                if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:@"ALERT"
                                          message:@"Please enable location services for this application in order to find hospitals near you."
                                          delegate:self
                                          cancelButtonTitle:@"Continue"
                                          otherButtonTitles:nil, nil];
                    [alert show];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:@"ALERT"
                                          message:@"No hospitals were found within the specified range."
                                          delegate:self
                                          cancelButtonTitle:@"Continue"
                                          otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }
    }]];
}

//Get SODAProc, HospitalGeneral, and HospitalScore for the given SODAHospital hosp
- (void)getInfoForHospital:(SODAHospital*)hosp {

    //Get Total Score data set
    __block NSArray *totalScores;
    
    SODAConsumer *consumer3 = [SODAConsumer consumerWithDomain:@"data.medicare.gov" token:nil];
    SODAQuery *query3 = [SODAQuery queryWithDataset:@"ypbt-wvdk" mapping:[SODAHospitalScore class]];
    
    
    NSLog(@"TEST provider number: %@", hosp.providerNumber);
    NSString *queryHospNum = [[NSString alloc] initWithFormat:@"'%@'", hosp.providerNumber];
    NSLog(@"TEST provider number222: %@", queryHospNum);
    
    [query3 where:@"provider_number" equals:queryHospNum];
    
    
    [consumer3 getObjectsForTypedQuery:query3 result:[SODACallback callbackWithResult:^(SODAResponse *response) {
        NSLog(@"json response GENERAL INFO : %@", response.json);
        if (response.hasError) {
            NSLog(@"\nError : %@", response.error.message);
        } else {
            
            totalScores = (NSArray*)response.entity;
            
            
            NSLog(@"ResultInside : %@", totalScores);
            NSLog(@"ResultInsideCount : %lu", (unsigned long)totalScores.count);
            
            SODAHospitalScore *thisScore;
            
            if (totalScores.count == 0) {
                //say there's no info
                thisScore = [SODAHospitalScore alloc];
//                thisScore.hospitalName = @"No information available";
            }
            else if (totalScores.count == 1) {
                thisScore = [totalScores objectAtIndex:0];
            }
            else {
                //idk this data is broken
                thisScore = [SODAHospitalScore alloc];
                //                generalInfo.coveredCharges = @"No information available and we are borked\n\n\n";
            }
            
            //Add the general info SODA class to SODAHospital
            hosp.score = thisScore;
            
            NSLog(@"TEST: %@", hosp.score);
        }
    }]];

    //
    //
    //
    
    //Begin function code
    //Get HospitalGeneralInfo
    __block NSArray *resultArray;

    SODAConsumer *consumer = [SODAConsumer consumerWithDomain:@"data.medicare.gov" token:nil];
    SODAQuery *query = [SODAQuery queryWithDataset:@"xubh-q36u" mapping:[SODAHospitalGeneral class]];
    
    
    NSLog(@"TEST provider number: %@", hosp.providerNumber);
    /*NSString **/queryHospNum = [[NSString alloc] initWithFormat:@"'%@'", hosp.providerNumber];
    NSLog(@"TEST provider number222: %@", queryHospNum);
    
    [query where:@"provider_id" equals:queryHospNum];

    
    [consumer getObjectsForTypedQuery:query result:[SODACallback callbackWithResult:^(SODAResponse *response) {
//        NSLog(@"json response GENERAL INFO : %@", response.json);
        if (response.hasError) {
            NSLog(@"\nError : %@", response.error.message);
        } else {
            
            resultArray = (NSArray*)response.entity;
            
            
//            NSLog(@"ResultInside : %@", resultArray);
//            NSLog(@"ResultInsideCount : %lu", (unsigned long)resultArray.count);
            
            SODAHospitalGeneral *generalInfo;
            
            if (resultArray.count == 0) {
                //say there's no info
                generalInfo = [SODAHospitalGeneral alloc];
                generalInfo.hospitalName = @"No information available";
            }
            else if (resultArray.count == 1) {
                generalInfo = [resultArray objectAtIndex:0];
            }
            else {
                //idk this data is broken
                generalInfo = [SODAHospitalGeneral alloc];
//                generalInfo.coveredCharges = @"No information available and we are borked\n\n\n";
            }
            
            //Add the general info SODA class to SODAHospital
            hosp.generalInfo = generalInfo;
            
            NSLog(@"TEST: %@", hosp.generalInfo);
        }
    }]];
 //end function code
    
    
    //new new new 7/19
    __block NSArray *procsOut;
    
    SODAConsumer *consumerOut = [SODAConsumer consumerWithDomain:@"data.cms.gov" token:nil];
    SODAQuery *queryOut = [SODAQuery queryWithDataset:@"tr34-anpb" mapping:[SODAProcOut class]];
    
    NSString *queryHospNumOut = [[NSString alloc] initWithFormat:@"'%@'", hosp.providerNumber];
    NSMutableString *queryProcOut = [[NSMutableString alloc] initWithFormat:@"'%@'", _autoCompleteTextField.text ];
    
    //cut out chars that aren't code # or single quotes
    [queryProcOut deleteCharactersInRange:NSMakeRange(APC_LENGTH, [queryProcOut length] - APC_LENGTH - 1)];
    
    [queryOut where:@"apc" startsWith:queryProcOut];
    [queryOut where:@"provider_id" equals:queryHospNumOut];
    
    
    NSLog(@"qryProc:  %@", queryProcOut);
    NSLog(@"qryHosp: %@", queryHospNumOut);
    
    //    [consumerOut getObjectsInDataset:@"97k6-zzx3" forQuery:@"select * where provider_name = ST ELIZABETH MEDICAL CENTER NORTH"
    //                        mappingTo:[Earthquake class]
    //                           result:[SODACallback callbackWithResult:^(SODAResponse *response) {
    [consumerOut getObjectsForTypedQuery:queryOut result:[SODACallback callbackWithResult:^(SODAResponse *response) {
        NSLog(@"json response FOR COSTSout : %@", response.json);
        if (response.hasError) {
            NSLog(@"\nError : %@", response.error.message);
        } else {
            
            procsOut = (NSArray*)response.entity;
            
            
            //            NSLog(@"ResultInside : %@", procs);
            //            NSLog(@"ResultInsideCount : %lu", (unsigned long)procs.count);
            
            SODAProcOut *proc;
            
            if (procsOut.count == 0) {
                //say there's no info
                proc = [SODAProcOut alloc];
                proc.submittedCharges = @"No information available";
            }
            else if (procsOut.count == 1) {
                proc = [procsOut objectAtIndex:0];
            }
            else {
                //idk this data is broken
                proc = [SODAProcOut alloc];
                proc.submittedCharges = @"No information available and we are borked\n\n\n";
                NSLog(@"%@", procsOut);
            }
            
            hosp.procOut = proc;
            
            NSLog(@"covered: %@", hosp.proc.coveredCharges);
            //            hosp.costthing = proc.coveredCharges;
            
            
            _numProcsGot++;
            if (_numProcsGot == _numProcsToGet) {
                
                NSLog(@"DOING MAP SEGUE for PROCS INPATIENT");
                
                [self performSegueWithIdentifier:@"search_segue" sender:self];
            }
            else {
                NSLog(@"DO NOT DO THE MAP SEGUE");
                NSLog(@"%ld %ld", (long)_numProcsGot, (long)_numProcsToGet);
            }

        }
    }]];
    
    
    //old good code
    
    
    __block NSArray *procs;
    
    SODAConsumer *consumer2 = [SODAConsumer consumerWithDomain:@"data.cms.gov" token:nil];
    SODAQuery *query2 = [SODAQuery queryWithDataset:@"97k6-zzx3" mapping:[SODAProc class]];
    
    NSString *queryHospNum2 = [[NSString alloc] initWithFormat:@"'%@'", hosp.providerNumber];
    NSMutableString *queryProc = [[NSMutableString alloc] initWithFormat:@"'%@'", _autoCompleteTextField.text ];
    
    [queryProc replaceOccurrencesOfString:@"&" withString:@"%26" options:0 range:NSMakeRange(0, [queryProc length])];

    
    [query2 where:@"drg_definition" equals:queryProc];
    [query2 where:@"provider_id" equals:queryHospNum2];
    
    
    NSLog(@"qryProc:  %@", queryProc);
    NSLog(@"qryHosp: %@", queryHospNum2);
    
//    [consumer2 getObjectsInDataset:@"97k6-zzx3" forQuery:@"select * where provider_name = ST ELIZABETH MEDICAL CENTER NORTH"
//                        mappingTo:[Earthquake class]
//                           result:[SODACallback callbackWithResult:^(SODAResponse *response) {
    [consumer2 getObjectsForTypedQuery:query2 result:[SODACallback callbackWithResult:^(SODAResponse *response) {
        NSLog(@"json response FOR COSTSin : %@", response.json);
        if (response.hasError) {
            NSLog(@"\nError : %@", response.error.message);
        } else {
            
            procs = (NSArray*)response.entity;
            
            
//            NSLog(@"ResultInside : %@", procs);
//            NSLog(@"ResultInsideCount : %lu", (unsigned long)procs.count);
            
            SODAProc *proc;
            
            if (procs.count == 0) {
                //say there's no info
                proc = [SODAProc alloc];
                proc.coveredCharges = @"No information available";
            }
            else if (procs.count == 1) {
                 proc = [procs objectAtIndex:0];
            }
            else {
                //idk this data is broken
                proc = [SODAProc alloc];
                proc.coveredCharges = @"No information available and we are borked\n\n\n";
                NSLog(@"%@", procs);
            }
            
            hosp.proc = proc;
            
            NSLog(@"covered: %@", hosp.proc.coveredCharges);
//            hosp.costthing = proc.coveredCharges;
        }
        

        
        
        _numProcsGot++;
        if (_numProcsGot == _numProcsToGet) {
            
            NSLog(@"DOING MAP SEGUE for PROCS INPATIENT");
            
            [self performSegueWithIdentifier:@"search_segue" sender:self];
        }
        else {
            NSLog(@"DO NOT DO THE MAP SEGUE");
            NSLog(@"%ld %ld", (long)_numProcsGot, (long)_numProcsToGet);
        }
    }]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"search_segue"]) {
        EPMapViewController  *vc = [segue destinationViewController];
        
        NSLog(@"Doing search_segue");
        
        vc.hospitals = (NSMutableArray*)_hospitals;
        vc.locationManager = _locationManager;
        vc.mapRadius = _range;
        
        
        //Close side VC if open
        SWRevealViewController *sw = (SWRevealViewController *)self.parentViewController.parentViewController;
        if ( sw.rearViewController.isViewLoaded && sw.rearViewController.view.window)
        {
            [sw revealToggle:self];
        }
        
        
        //Stop spinner
        [_indicator stopAnimating];
    }
    else if ([segue.identifier isEqualToString:@"main_about"]) {
        
        [self.view endEditing:YES];
        
        NSLog(@"Doing about segue");
    }
    else {
        NSLog(@"error, some failing segue from epViewController");
    }
}

- (double)radiansFromDegrees:(double)degrees
{
    return degrees * (M_PI/180.0);
}

- (double)degreesFromRadians:(double)radians
{
    return radians * (180.0/M_PI);
}

- (CLLocationCoordinate2D)coordinateFromCoord:(CLLocationCoordinate2D)fromCoord
                                 atDistanceKm:(double)distanceKm
                             atBearingDegrees:(double)bearingDegrees
{
    double distanceRadians = distanceKm / 6371.0; //6,371 = Earth's radius in km
    double bearingRadians = [self radiansFromDegrees:bearingDegrees];
    double fromLatRadians = [self radiansFromDegrees:fromCoord.latitude];
    double fromLonRadians = [self radiansFromDegrees:fromCoord.longitude];
    
    double toLatRadians = asin( sin(fromLatRadians) * cos(distanceRadians)
                               + cos(fromLatRadians) * sin(distanceRadians) * cos(bearingRadians) );
    
    double toLonRadians = fromLonRadians + atan2(sin(bearingRadians)
                                                 * sin(distanceRadians) * cos(fromLatRadians), cos(distanceRadians)
                                                 - sin(fromLatRadians) * sin(toLatRadians));
    
    // adjust toLonRadians to be in the range -180 to +180...
    toLonRadians = fmod((toLonRadians + 3*M_PI), (2*M_PI)) - M_PI;
    
    CLLocationCoordinate2D result;
    result.latitude = [self degreesFromRadians:toLatRadians];
    result.longitude = [self degreesFromRadians:toLonRadians];
    return result;
}


- (IBAction)doOptimization:(id)sender {
    
    if ( [_autoCompleteTextField.text isEqualToString:@""] ) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"ALERT"
                              message:@"Please enter an Elective Procedure in the text box."
                              delegate:self
                              cancelButtonTitle:@"Continue"
                              otherButtonTitles:nil, nil];
        [alert show];
        return;
    }

    //Do Socrate/SODA calls
    //Callback runSolver is called after all complete
    //[self getHospitals];
    
    switch (_searchRadius.selectedSegmentIndex) {
        case 0:
            _range = 10;
            break;
        case 1:
            _range = 25;
            break;
        case 2:
            _range = 50;
            break;
        case 3:
            _range = 100;
            break;
        default:
            _range = 50;
            break;
    }
    
    
    UIButton *but = (UIButton*)sender;
    
    int indicatorSize = 40;
    
    //Put a spinner on the screen while work is being done
    CGRect b = but.frame;
//    CGRect screen = self.view.bounds;

    
    _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //center the indicator in the view
    _indicator.frame = CGRectMake(b.origin.x + b.size.width/2 - indicatorSize/2, b.origin.y - indicatorSize, indicatorSize, indicatorSize);
    [self.view addSubview: _indicator];
//    [_indicator release];
    [_indicator setHidden:NO];
    [_indicator startAnimating];
    
    [self getHospitalsWithinRange];
    
}

/*
- (void)sodaQuery:(SODAQuery*)query
     withConsumer:(SODAConsumer*)consumer
      forHospital:(SODAHospital*)hospi

{
    __block NSArray *resultArray;
    
    
    
    [consumer getObjectsForTypedQuery:query result:[SODACallback callbackWithResult:^(SODAResponse *response) {
        if (response.hasError) {
            NSLog(@"QUERY Error : %@", response.error.message);
        } else {
            resultArray = (NSArray*)response.entity;
            
            
            NSLog(@"QUERY ResultInside : %@", resultArray);
            NSLog(@"QUERY ResultInsideCount : %lu", (unsigned long)resultArray.count);
            
            SODAHospitalGeneral *generalInfo;
            
            //No results received
            if (resultArray.count == 0) {
                generalInfo = [SODAHospitalGeneral alloc];
                generalInfo.hospitalName = @"No information available";
            }
            //Perfect! Got 1 result
            else if (resultArray.count == 1) {
                generalInfo = [resultArray objectAtIndex:0];
            }
            //Uh oh, not sure what to do
            //TODO
            //Might need some work
            else {
                //idk this data is broken
                generalInfo = [SODAHospitalGeneral alloc];
            }
            
            //Add the general info SODA class to SODAHospital
            hospi.generalInfo = generalInfo;
            //TODO make sure this assignment is actually working
            
            NSLog(@"TEST: %@", hospi.generalInfo);
        }
    }]];
}
*/




///////////////////////////////////////
/////GLPK SOLVER CODE (EVENTUALLY)/////
///////////////////////////////////////
//
//
//Call this at the end of each asynchronous SODA call we need to make (for solver)
//Runs the solver after all are finished
//- (void)sodaHandler {
//    NSLog(@"callsRemaining1  %ld", (long)_sodaCallsRemaining);
//
//    if (--_sodaCallsRemaining <= 0)
//    {
//        [self runSolver];
//    }
//}

//Runs the solver after all SODA calls finish
//Find the best (eventually best n) hospitals given the data and user's constraints
//- (void)runSolver {
//
//    //NSArray *localHospitals = (SODAHospital*)_hospitals;
//
//    NSLog(@"ResultinRunSolver : %lu", (unsigned long)_hospitals.count);
//    SODAHospital *myH = [_hospitals firstObject] ;
//
//    NSLog(@"%@", myH.city);
//
//    //below this line is solver code
//
//    //TODO get this value properly...from APIs
//    int numHospitals = (int)_hospitals.count;
//
//    //printf("this is a test: %d", _constraintOne.selectedSegmentIndex);
//    printf("GLPK Version: %s\n", glp_version());
//
//    glp_prob* lp = glp_create_prob();
//    glp_set_prob_name(lp, "Elective Procedure Optimization");
//    glp_set_obj_dir(lp, GLP_MIN);
//
//    //Rows: Auxiliary variables/constraints
//    glp_add_rows(lp, 7);
//
//    //TODO get these values from the user...how?
//    //Set constraints for the model
//    glp_set_row_name(lp, 1, "totalScore");          //this means the totalScore should be greater than 73
//    glp_set_row_bnds(lp, 1, GLP_LO, 0, 73);
//    glp_set_row_name(lp, 2, "overallHospitalRating"); //and so on
//    glp_set_row_bnds(lp, 2, GLP_LO, 0, 9);
//    glp_set_row_name(lp, 3, "communicationNurse");
//    glp_set_row_bnds(lp, 3, GLP_LO, 0, 7);
//    glp_set_row_name(lp, 4, "communicationDoctor");
//    glp_set_row_bnds(lp, 4, GLP_LO, 0, 3);
//    glp_set_row_name(lp, 5, "staffResponse");
//    glp_set_row_bnds(lp, 5, GLP_LO, 0, 5);
//    glp_set_row_name(lp, 6, "communicationMedicine");
//    glp_set_row_bnds(lp, 6, GLP_LO, 0, 6);
//    glp_set_row_name(lp, 7, "dischargeInfo");
//    glp_set_row_bnds(lp, 7, GLP_LO, 0, 9);
//
//    //Rows: Structural Variables. 1 for the chosen hospital, 0 otherwise
//    glp_add_cols(lp, numHospitals);
//    for (int i=1; i<=numHospitals; i++)
//    {
//        //glp_set_col_bnds(lp, i, GLP_DB, 0.0, 1.0);
//        glp_set_col_kind(lp, i, GLP_BV); //Binary Variable
//    }
//
//    //TODO set nonzeros (all?) of constraint matrix
//    int numConstraintMatrixVals = numHospitals*NUM_SOLVER_CONSTRAINTS;
//    int ia[numConstraintMatrixVals], ja[numConstraintMatrixVals];
//    double ar[numConstraintMatrixVals];
//
//    int index = 0;
//    for (int i=0; i<NUM_SOLVER_CONSTRAINTS; i++) {
//        for (int j=0; j<numHospitals; j++, index++) {
//            ia[index] = i;
//            ja[index] = j;
//        }
//    }
//    for (int i=0; i<numHospitals; i++) {
//        myH = [_hospitals objectAtIndex:i];
//
//            int start = i*NUM_SOLVER_CONSTRAINTS;
//
//            char tempC = [myH.overallRatingOfHospitalDimensionScore characterAtIndex:0];
//
//            //ar[start] = [[myH.overallRatingOfHospitalDimensionScore stringWithFormat:@"%c" ] intValue];
//        ar[start] = [[NSString stringWithFormat:@"%c", tempC] intValue];
//
//        NSLog(@"ar:  %f    tempC:  %c", ar[start], tempC);
//
//
////            ar[start + 1] = ;
////            ar[start + 2] = ;
////            ar[start + 3] = ;
////            ar[start + 4] = ;
////            ar[start + 5] = ;
////            ar[start + 6] = ;
//
////        }
//    }
//    //glp_load_matrix(lp, numConstraintMatrixVals, ia, ja, ar);
//
//    return;
//}

//- (void)getHospitals {
//    //affect global hospitals array
//
//    SODAConsumer *consumer = [SODAConsumer consumerWithDomain:@"data.medicare.gov" token:nil];
//
//
//    [consumer getObjectsInDataset:@"avtz-f2ge" forQuery:@"select * where state='OH'"
//                        mappingTo:[SODAHospital class]
//                           result:[SODACallback callbackWithResult:^(SODAResponse *response) {
//
//        NSLog(@"json response : %@", response.json);
//        if (response.hasError) {
//            NSLog(@"\nError : %@", response.error.message);
//        } else {
//
//            _hospitals = (NSArray*)response.entity;
//            //NSLog(@"ResultInside : %@", _hospitals);
//            NSLog(@"ResultInsideCount : %lu", (unsigned long)_hospitals.count);
//
//            [self sodaHandler];
//        }
//    }]];
//
//    return;
//

@end
