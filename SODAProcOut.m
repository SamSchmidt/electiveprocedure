//
//  SODAProcOut.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 7/19/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//Outpatient Procedures

//https://data.cms.gov/Public-Use-Files/Outpatient-Prospective-Payment-System-OPPS-Provide/tr34-anpb
//tr34-anpb


#import "SODAProcOut.h"

/**
 * Model class where SODA Response results get mapped for each one of the matching hospitals
 */
@implementation SODAProcOut {
    
}

#pragma mark - Properties

@synthesize procedureName = _procedureName;
@synthesize submittedCharges = _submittedCharges;
@synthesize totalPayments = _totalPayments;

#pragma mark - SODAPropertyMapping custom mappings impl

/**
 * This custom mapping allows soda response properties to be mapped to object properties with names that do not match those on the
 * remote objects
 */
- (NSDictionary *)propertyMappings {
    return @{
             @"apc" : @"procedureName",
             @"average_estimated_submitted_charges" : @"submittedCharges",
             @"average_total_payments" : @"totalPayments",
             };
}

@end
