//
//  SODAProc.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 3/15/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
// https://data.cms.gov/Medicare/Inpatient-Prospective-Payment-System-IPPS-Provider/97k6-zzx3

#import <Foundation/Foundation.h>
#import "SODAPropertyMapping.h"

@interface SODAProc : NSObject<SODAPropertyMapping> {
    NSString *_procedureName;
    NSString *_coveredCharges;
    NSNumber *_averageMedicarePayments;
    NSNumber *_averageMedicarePayments2;
    
}

#pragma mark - Properties

@property(nonatomic, copy) NSString *procedureName;
@property(nonatomic, copy) NSString *coveredCharges;
@property(nonatomic, copy) NSNumber *averageMedicarePayments;
@property(nonatomic, copy) NSNumber *averageMedicarePayments2;


@end
