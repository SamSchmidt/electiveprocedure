////
////  EPMailChimpViewController.h
////  ElectiveProcedure3
////
////  Created by Samuel Schmidt on 8/24/14.
////  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//
//#import "Lib/ChimpKit/Core/Classes/SubscribeAlertView.h"
//#import "Lib/ChimpKit/Core/Classes/CKAuthViewController.h"
//#import "Lib/ChimpKit/Core/Classes/ChimpKit.h"
//
//
//@interface EPMailChimpViewController : UIViewController <CKAuthViewControllerDelegate> {
//    BOOL shownAuthView;
//}
//@end
