//
//  EPMapViewController.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 3/26/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "WYPopoverController.h"
#import "EPPopoverViewController.h"

@interface EPMapViewController : UIViewController <UISplitViewControllerDelegate, EPPopoverDelegate>

- (void)dismissPopupAndSave;


@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (weak, nonatomic) IBOutlet UILabel *procedureLabel;



@property (weak, nonatomic) IBOutlet UILabel *costMinLabel;
@property (weak, nonatomic) IBOutlet UILabel *costMaxLabel;





@property CLLocationManager *locationManager;
@property int mapRadius;


@property NSMutableArray *hospitals;
@property bool hospitalsAreValid;

//@property EPPopoverViewController* myPopoverController;

//popover information
@property WYPopoverController* aPopover;
@property MKAnnotationView* aPopView;
@property CGRect aPopRect;
@property WYPopoverArrowDirection aPopArrow;
@property MKAnnotationView* popupView;


@property BOOL returnFromInfoPage;

@property NSMutableArray *arr;

@end
