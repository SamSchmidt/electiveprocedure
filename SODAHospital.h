//
//  SODAHospital.h
//  ElectiveProcedure
//
//  Created by Samuel Schmidt on 1/27/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//https://data.medicare.gov/Hospital-Compare/Hospital-Value-Based-Purchasing-HVBP-Patient-Exper/avtz-f2ge
//avtz-f2ge

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SODAPropertyMapping.h"

#import "SODAProc.h"
#import "SODAProcOut.h"
#import "SODAHospitalGeneral.h"
#import "SODAHospitalScore.h"

@class SODALocation;

/**
 * Model class where SODA Response results get mapped for each one of the matching earthquakes
 */
@interface SODAHospital : NSObject<SODAPropertyMapping> {
    NSString *_providerNumber;
    NSString *_hospitalName;
    NSString *_address;
    NSString *_city;
    NSString *_state;
    NSString *_zipCode;
    NSString *_countyName;
    
    //all are on a scale of 0-10
    NSString *_communicationWithNursesDimensionScore;
    NSString *_communicationWithDoctorsDimensionScore;
    NSString *_responsivenessOfHospitalStaffDimensionScore;
    NSString *_communicationAboutMedicinesDimensionScore;
    NSString *_dischargeInformationDimensionScore;
    NSString *_overallRatingOfHospitalDimensionScore;
    
    //out of 80
    NSNumber *_overallScore;
    
    SODALocation *_location;
}

#pragma mark - Properties

@property(nonatomic, copy) NSString *providerNumber;
@property(nonatomic, copy) NSString *hospitalName;
@property(nonatomic, copy) NSString *address;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *state;
@property(nonatomic, copy) NSString *zipCode;
@property(nonatomic, copy) NSString *countyName;

@property(nonatomic, copy) NSString *communicationWithNursesDimensionScore;
@property(nonatomic, copy) NSString *communicationWithDoctorsDimensionScore;
@property(nonatomic, copy) NSString *responsivenessOfHospitalStaffDimensionScore;
@property(nonatomic, copy) NSString *communicationAboutMedicinesDimensionScore;
@property(nonatomic, copy) NSString *dischargeInformationDimensionScore;
@property(nonatomic, copy) NSString *overallRatingOfHospitalDimensionScore;

@property(nonatomic, strong) NSNumber *overallScore;

@property(nonatomic, strong) SODALocation *location;

//not SODA properties
@property NSInteger ranking;
@property SODAProc *proc;
@property SODAProcOut *procOut;
@property SODAHospitalGeneral *generalInfo;
@property SODAHospitalScore *score;


//@property NSString *costthing;
@end
