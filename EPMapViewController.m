//
//  EPMapViewController.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 3/26/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import "EPMapViewController.h"
#import "EPLocation.h"
#import "EPPopoverViewController.h"
#import "WYPopoverController.h"
#import "WYStoryboardPopoverSegue.h"
#import "ZSPinAnnotation.h"

#import "SODALocation.h"
#import "SODAHospital.h"

#import "EPInfoTableViewController.h"

#define METERS_PER_MILE 1609.344

#define MAX_ANNOTATIONS 15

@interface EPMapViewController ()

@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;

@property NSInteger annotationsGraphed;

@property double costMin;
@property double costMax;


@end

@implementation EPMapViewController

@synthesize hospitals;
@synthesize hospitalsAreValid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    // 1
    CLLocationCoordinate2D zoomLocation;
    
    zoomLocation.latitude = _locationManager.location.coordinate.latitude;
    zoomLocation.longitude = _locationManager.location.coordinate.longitude;
    
    
    // 2
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation,
//                                                                       1.3*_mapRadius*METERS_PER_MILE, 1.5*_mapRadius*METERS_PER_MILE);
//    
    
    // 3
//    [_mapView setRegion:viewRegion animated:YES];
    
    _annotationsGraphed = 0;
    
    
    _returnFromInfoPage = false;

    
    _mapView.delegate=(id)self;
    
    [self plotHospitalPositions];
    
    
    
    [_mapView showAnnotations:_mapView.annotations animated:true];
    NSLog(@"%lu", (unsigned long)[_mapView.annotations count]);
}

- (void)viewWillAppear:(BOOL)animated {

    
//    _red initWithObjects:0, 1, 2;
}

- (void)viewDidAppear:(BOOL)animated {
//    NSLog(@"In mapViewController viewDidAppear");
    

    

//    if (_returnFromInfoPage) {
//        //plot popup
//        
//        [self.mapView selectAnnotation:_aPopView.annotation animated:true];
////        [self mapView:self.mapView didSelectAnnotationView:(MKAnnotationView*)_aPopView];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //    UINavigationController *nav = [segue destinationViewController];
    //    EPMapViewController *vc = (EPMapViewController*)nav.topViewController;
    //    EPMapViewController  *vc = [segue destinationViewController];
    
    if ([segue.identifier isEqual:@"mapToInfoPageSegue"])
    {
    
        NSLog(@"PREPARE for info segue");
        
        EPPopoverViewController *senderVC = (EPPopoverViewController*)sender;
        EPInfoTableViewController  *destVC = [segue destinationViewController];
        destVC.hospital = senderVC.hospital;
        
        
        NSLog(@"sender:%@", senderVC.hospital.hospitalName);
        NSLog(@"\n");
        
        
        [_aPopover dismissPopoverAnimated:TRUE];
    }
}


- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered
{
    if (_returnFromInfoPage) {
        //plot popup
        [mapView selectAnnotation:_aPopView.annotation animated:true];
    }
}



// Add new method above refreshTapped
- (void)plotHospitalPositions/*:(NSArray *)hospitalArr*/ {
    for (id<MKAnnotation> annotation in _mapView.annotations) {
        [_mapView removeAnnotation:annotation];
    }
    
//    NSDictionary *root = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
//    NSArray *data = [root objectForKey:@"data"];
    
    
    //plot user's location annotation (but we don't want to draw the pin)
    CLLocation *loc =  [_locationManager location];
    CLLocationCoordinate2D coordinate = loc.coordinate;
    EPLocation *annotation = [[EPLocation alloc] initWithName:@"User" address:nil cost:0 coordinate:coordinate];
    [_mapView addAnnotation:annotation];
    
    //Sort hospitals by cost
//    SODAHospital *h = [hospitals objectAtIndex:0];
    
    NSMutableArray *toDelete = [[NSMutableArray alloc] init];
    NSString *sortByKey = [[NSString alloc] init];
    for ( SODAHospital *h in hospitals ) {
        if ( h.proc.procedureName == nil) {
            if ( h.procOut.procedureName == nil) {
                [toDelete addObject:h];
                continue;
            }
            sortByKey = @"procOut.submittedCharges";
            continue;
        }
        sortByKey = @"proc.coveredCharges";
    }
    
    [hospitals removeObjectsInArray:toDelete];
    
    
    //TODO: do this properly, w/o sortkey (check h[0] for which property is there
    if ( [hospitals count] > 0 ) {
        if ( [sortByKey isEqualToString:@"procOut.submittedCharges"] ) {
            SODAHospital *h = [hospitals objectAtIndex:0];
            [_navigationBar setTitle:[h.procOut.procedureName substringFromIndex:7] ];
            
            _procedureLabel.text = [h.procOut.procedureName substringFromIndex:7];
        }
        else {
            SODAHospital *h = [hospitals objectAtIndex:0];
            [_navigationBar setTitle:[[h.proc.procedureName substringFromIndex:6] capitalizedString] ];
            
            _procedureLabel.text = [[h.proc.procedureName substringFromIndex:6] capitalizedString];
        }
        [_navigationBar.titleView sizeToFit];
        
        
        _procedureLabel.adjustsFontSizeToFitWidth = YES;
    }
    else {
        //this means we filtered out all hospitals, we shouldn't show
        //the map page
        //TODO
        //Fix this
        NSLog(@"mapview 195 here");
    }
    
    
//    
//    NSSortDescriptor *sd;
//    
//    //TODO: ascending appropriate for new color scheme?
//    sd = [[NSSortDescriptor alloc] initWithKey:sortByKey ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sd];
////    NSArray *sortedArray;
//    hospitals = (NSMutableArray*)[hospitals sortedArrayUsingDescriptors:sortDescriptors];
    
    
    
    //initialize cost variables
    double cost;
    _costMax=0.0, _costMin=-1.0;
    
    for (SODAHospital *hosp in hospitals) {
        
        if ( hosp.proc.procedureName != nil ) {
            cost = [hosp.proc.coveredCharges doubleValue];
        }
        else if ( hosp.procOut.procedureName != nil ) {
            cost = [hosp.procOut.submittedCharges doubleValue];
        }
        else {
            NSLog(@"Did not plot hospital: %@", hosp.hospitalName);
            continue;
        }
        
        //check for new max and min cost
        if ( cost > _costMax ) { _costMax = cost; }
        if ( cost < _costMin || _costMin < 0) { _costMin = cost; }

        
        
        //filter out hospitals with no information (don't display)
        
        SODALocation *loc = hosp.location;
        
        NSNumber * latitude = loc.latitude;
        NSNumber * longitude = loc.longitude;
        NSString * name = hosp.hospitalName;
        NSString * address = [NSString stringWithFormat:@"%@, %@, %@", hosp.address, hosp.city, hosp.state];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = latitude.doubleValue;
        coordinate.longitude = longitude.doubleValue;
        EPLocation *annotation = [[EPLocation alloc] initWithName:name address:address cost:cost coordinate:coordinate];
        [_mapView addAnnotation:annotation];
        
        NSLog(@"Name: %@", name);
        NSLog(@"Original location %@ %@", loc.latitude, loc.longitude);
        NSLog(@"New mapped location %f, %f", coordinate.latitude, coordinate.longitude);
    }
    
    //Formatting work for min/max labels
    NSNumber *max = [[NSNumber alloc] initWithDouble:_costMax];
    NSNumber *min = [[NSNumber alloc] initWithDouble:_costMin];

    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    
    [nf setGroupingSeparator:@","];
    [nf setGroupingSize:3];
    [nf setUsesGroupingSeparator:YES];
    [nf setMaximumFractionDigits:0];
    
    
    //Display min and max values on appropriate labels
    _costMinLabel.text = [NSString stringWithFormat:@"$%@", [nf stringFromNumber:min]];

    _costMaxLabel.text = [NSString stringWithFormat:@"$%@", [nf stringFromNumber:max]];

}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id)annotation {
    //7
//    if([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
    
//    else
    if ( ([annotation isKindOfClass:[EPLocation class]]) ) {
        EPLocation *ann = (EPLocation *)annotation;
        if ( [[ann title] isEqualToString:@"User"]) {
            MKAnnotationView *view = [[MKAnnotationView alloc] init];
            //view.image = nil;
            return view;
        }
        //8
        static NSString *identifier = @"myAnnotation";
        ZSPinAnnotation *annotationView = (ZSPinAnnotation*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (!annotationView)
        {
            NSInteger numAnnotations = MIN( [hospitals count], MAX_ANNOTATIONS );
            
            CGFloat red, green, blue;
            
            double ratio = (ann.cost - _costMin)/(_costMax - _costMin);
            
            if ( ratio < 0.5 ) {
            
                red = (2.0 * ratio);
                green = 1.0;
                blue = red;
            }
            else {
                red = 1.0;
                green = 1 - (2 * (ratio - 0.5));
                blue = green;
            }
            
            
            NSLog(@"\ngraphed: %ld\nnumAnnotations: %ld", (long)_annotationsGraphed, (long)numAnnotations);
            NSLog(@"\nRED: %f\nGREEN: %f\nBLUE: %f", red, green, blue);
            
            
            //9
            annotationView = [[ZSPinAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            
//            annotationView.annotationType = ZSPinAnnotationTypeStandard;
//            annotationView.annotationType = ZSPinAnnotationTypeDisc;
            annotationView.annotationType = ZSPinAnnotationTypeTag;

            annotationView.annotationColor = [UIColor colorWithRed:red
                                                             green:green
                                                              blue:0
                                                             alpha:1];
            


            _annotationsGraphed++;
            
//            annotationView.pinColor = MKPinAnnotationColorPurple;
//            annotationView.animatesDrop = YES;
//            annotationView.canShowCallout = YES;
        }
        else {
            annotationView.annotation = annotation;
        }
        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
//        annotationView.bounds
    
        
//        CGRect r = CGRectMake(annotationView.bounds.origin.x, annotationView.bounds.origin.y, 100, 100);
//        UILabel *label = [[UILabel alloc] initWithFrame:r];
        
//newnewnew
            
        annotationView.canShowCallout = NO;
//        annotationView.image = [UIImage imageNamed:@"menu.png"];
        
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    [mapView deselectAnnotation:view.annotation animated:YES];
    
    EPLocation *ann = (EPLocation *)view.annotation;
    if ( [[ann title] isEqualToString:@"User"] ) {
        return;
    }
    
    //Color pin yellow and save the current color
    ZSPinAnnotation *pinView = (ZSPinAnnotation*)view;
    [pinView setAnnotationColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]]; //saves current color
    
    CGRect rect;
    CGSize size; size.height = 0; size.width = 0;

    rect.size = size;
    
    WYPopoverArrowDirection arrowDirection;
    if (view.center.y > _mapView.bounds.size.height/2) {
        arrowDirection = WYPopoverArrowDirectionDown;
        rect.origin.y = view.center.y-37;
        rect.origin.x = view.center.x+1;
    }
    else {
        arrowDirection = WYPopoverArrowDirectionUp;
        rect.origin.y = view.center.y+2;
        rect.origin.x = view.center.x+1;
    }
    
    NSString *nameToMatch = view.annotation.title;
    
    NSInteger index = -1;
    index = [hospitals indexOfObjectPassingTest:
                             ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            SODAHospital *h = (SODAHospital*)obj;
            
            if ([h.hospitalName isEqualToString:nameToMatch]) {
                *stop = YES;
                return YES;
            }
            return NO;
        }];
    
    NSLog(@"index of hospital:   %ld", (long)index);
    
//    if (index < 0)
//        NSLog(@"Found the wrong hospital :( ");
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
//    CGFloat screenHeight = screenRect.size.height;
    
    EPPopoverViewController *contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"popoverViewController"];
    
    //Set up popover (VC) size
    CGSize mySize;
    mySize.height = 200;
    mySize.width = screenWidth;
    
    contentViewController.preferredContentSize = mySize;
    
    
    
    @try {
        [contentViewController addHospital:[hospitals objectAtIndex:index]];
    }
    @catch ( ... ) {
//        NSLog(@"Exception: %@", e);
        NSLog(@"No popup should show, but continue execution");
        
        return;
    }
    @finally {
        // Added to show finally works as well
    }
    
    
    
    [contentViewController setMapViewController:self];
//    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"popoverViewController"]];
    
    _aPopover = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
//    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    
    _aPopover.delegate = (id)self;
    
    _aPopover.theme = [WYPopoverTheme themeForIOS7];
    
    
    
//    _aPopover.theme.borderWidth = 100;
//
//    contentViewController.title = @"lakjsdflkasjdf";

    
//    [_aPopover beginThemeUpdates];
//        _aPopover.theme.arrowHeight = 100;
//        _aPopover.theme.arrowBase = 25;
//    [_aPopover endThemeUpdates];


//    contentViewController.preferredContentSize = CGSizeMake(300, 300);


//    _aPopover.passthroughViews = @[btn];
//    _aPopover.popoverLayoutMargins = UIEdgeInsetsMake(10, 10, 10, 10);
//    _aPopover.wantsDefaultContentAppearance = YES;
//    [_aPopover setPopoverContentSize:size];
    
    
//    WYPopoverTheme *theme =
//    theme.tintColor = [UIColor redColor];
//

//    UIColor *greenColor = [UIColor colorWithRed:26.f/255.f green:188.f/255.f blue:156.f/255.f alpha:1];
//    
//    [WYPopoverController setDefaultTheme:[WYPopoverTheme theme]];
//    
//    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
//    
//    [popoverAppearance setOuterCornerRadius:4];
//    [popoverAppearance setOuterShadowBlurRadius:0];
//    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
//    [popoverAppearance setOuterShadowOffset:CGSizeMake(0, 0)];
//    
//    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
//    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
//    
//    [popoverAppearance setBorderWidth:8];
//    [popoverAppearance setArrowHeight:10];
//    [popoverAppearance setArrowBase:20];
//    
//    [popoverAppearance setInnerCornerRadius:4];
//    [popoverAppearance setInnerShadowBlurRadius:0];
//    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
//    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
//    
//    [popoverAppearance setFillTopColor:greenColor];
//    [popoverAppearance setFillBottomColor:greenColor];
//    [popoverAppearance setOuterStrokeColor:greenColor];
//    [popoverAppearance setInnerStrokeColor:greenColor];
//    
//    UINavigationBar* navBarInPopoverAppearance = [UINavigationBar appearanceWhenContainedIn:[UINavigationController class], [WYPopoverController class], nil];
//    [navBarInPopoverAppearance setTitleTextAttributes: @{
//                                                         UITextAttributeTextColor : [UIColor whiteColor],
//                                                         UITextAttributeTextShadowColor : [UIColor clearColor],
//                                                         UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetMake(0, -1)]}];
//    
    
//    UINavigationBar* navBarInPopoverAppearance = [UINavigationBar appearanceWhenContainedIn:[UINavigationController class], [WYPopoverController class], nil];
//    [navBarInPopoverAppearance setTitleTextAttributes: @{
//                UITextAttributeTextColor : [UIColor whiteColor],
//                UITextAttributeTextShadowColor : [UIColor clearColor],
//                UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetMake(0, -1)]}];
//    
    
    
    //pinView.savedColor = pinView.annotationColor;
    //save the annotation so that we can revert the color later
    _aPopover.annotationView = pinView;
    
    
    [_aPopover presentPopoverFromRect:rect
                               inView:view.superview
            permittedArrowDirections:arrowDirection
                             animated:YES];
    
    //save popover information for reuse (bringing back the popover when returning from info page)
    _aPopArrow = arrowDirection;
    _aPopRect = rect;
    
    if (_aPopView == nil)
        _aPopView = (MKAnnotationView*)[[MKAnnotationView alloc] initWithAnnotation:(id)view reuseIdentifier:@"blah"];
    
//    _popupView = view;
}

- (void)dismissPopupAndSave {
    _returnFromInfoPage = true;
    
    [self popoverControllerShouldDismissPopover:_aPopover];

}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    [controller dismissPopoverAnimated:TRUE];
//    ZSPinAnnotation* pin = (ZSPinAnnotation*)controller.annotationView;
    ZSPinAnnotation* pin = controller.annotationView;
    [pin setAnnotationColor:pin.savedColor];

    return YES;
}

@end
