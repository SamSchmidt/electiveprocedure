//
//  SODAHospitalGeneral.m
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 6/14/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//
//General Hospital Information
//https://data.medicare.gov/Hospital-Compare/Hospital-General-Information/v287-28n3
//v287-28n3
//https://data.medicare.gov/Hospital-Compare/Hospital-General-Information/xubh-q36u
//xubh-q36u

#import "SODAHospitalGeneral.h"


@implementation SODAHospitalGeneral {

}

#pragma mark - Properties

@synthesize providerNumber = _providerNumber;
@synthesize hospitalName = _hospitalName;
@synthesize address = _address;
@synthesize city = _city;
@synthesize state = _state;
@synthesize zipCode = _zipCode;
@synthesize county = _county;
@synthesize phoneNumber = _phoneNumber;
@synthesize hospitalType = _hospitalType;
@synthesize hospitalOwner = _hospitalOwner;

#pragma mark - SODAPropertyMapping custom mappings impl

/**
 * This custom mapping allows soda response properties to be mapped to object properties with names that do not match those on the
 * remote objects
 */
- (NSDictionary *)propertyMappings {
    return @{
             @"provider_id" : @"providerNumber",
             @"hospital_name" : @"hospitalName",
             @"address" : @"address",
             @"city" : @"city",
             @"state" : @"state",
             @"zip_code" : @"zipCode",
             @"county_name" : @"county",
             @"phone_number" : @"phoneNumber",
             @"hospital_type" : @"hospitalType",
             @"hospital_ownership" : @"hospitalOwner",
             };
}

@end
