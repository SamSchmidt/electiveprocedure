//
//  EPPopoverViewController.h
//  ElectiveProcedure3
//
//  Created by Samuel Schmidt on 4/13/14.
//  Copyright (c) 2014 Samuel Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SODAHospital.h"
//#import "EPMapViewController.h"

@class EPMapViewController;

@protocol EPPopoverDelegate <NSObject>
@required
//-(void)selectedColor:(UIColor *)newColor;
@end

@interface EPPopoverViewController : UIViewController

@property (nonatomic, weak) id<EPPopoverDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;


@property (weak, nonatomic) IBOutlet UILabel *labelOne;
@property (weak, nonatomic) IBOutlet UILabel *labelTwo;

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property EPMapViewController* backgroundMapController;

@property SODAHospital* hospital;

- (id)addHospital:(SODAHospital *)hosp;
- (void)setMapViewController:(EPMapViewController *)mvc;


//Evnet handlers
- (IBAction)buttonTouchDown:(id)sender;
- (IBAction)buttonTouchUpOutside:(id)sender;


- (IBAction)infoButtonTouchUpInside:(id)sender;
- (IBAction)callButtonTouchUpInside:(UIButton *)sender;
- (IBAction)directionsButtonTouchUpInside:(id)sender;


@end
